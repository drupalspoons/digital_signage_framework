<?php

namespace Drupal\digital_signage_framework\Event;

use Drupal\digital_signage_device\DeviceInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class CssFiles
 *
 * @package Drupal\digital_signage_framework\Event
 */
class CssFiles extends Event {

  /**
   * @var array
   */
  protected $files = [];

  /**
   * @var \Drupal\digital_signage_device\DeviceInterface
   */
  protected $device;

  /**
   * CssFiles constructor.
   *
   * @param \Drupal\digital_signage_device\DeviceInterface $device
   */
  public function __construct(DeviceInterface $device) {
    $this->device = $device;
  }

  /**
   * @return array
   */
  public function getFiles(): array {
    return $this->files;
  }

  /**
   * @param string $file
   *
   * @return \Drupal\digital_signage_framework\Event\CssFiles
   */
  public function addFile($file): CssFiles {
    $this->files[] = $file;
    return $this;
  }

  /**
   * @return \Drupal\digital_signage_device\DeviceInterface
   */
  public function getDevice(): DeviceInterface {
    return $this->device;
  }

}
