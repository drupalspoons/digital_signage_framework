<?php

namespace Drupal\digital_signage_framework;

/**
 * Contains all events thrown by the DigitalSignageFramework module.
 */
final class DigitalSignageFrameworkEvents {

  public const CSSFILES = 'cssfiles';

  public const RENDERED = 'rendered';

}
