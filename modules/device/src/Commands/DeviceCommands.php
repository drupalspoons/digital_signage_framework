<?php

namespace Drupal\digital_signage_device\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\digital_signage_device\Entity\Device;
use Drupal\digital_signage_platform\PlatformPluginManager;
use Drush\Commands\DrushCommands;
use InvalidArgumentException;

/**
 * A Drush commandfile for digital signage devices.
 */
class DeviceCommands extends DrushCommands {

  /**
   * @var \Drupal\digital_signage_platform\PlatformPluginManager
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(PlatformPluginManager $plugin_manager) {
    parent::__construct();
    $this->pluginManager = $plugin_manager;
  }

  /**
   * Synchronize devices with all platforms.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option platform
   *   Platform ID if only one should be synchronised, defaults to all.
   * @usage digital-signage-device:sync
   *   Synchronises all devices with all enabled platforms.
   *
   * @command digital-signage-device:sync
   * @aliases dsds
   */
  public function syncDevices($options = ['platform' => NULL]) {
    $this->pluginManager->syncDevices($options['platform']);
  }

  /**
   * Show debug log.
   *
   * @usage digital-signage-device:log:debug 17
   *   Show debug logs from device with id 17.
   *
   * @command digital-signage-device:log:debug
   * @param int $deviceId
   * @option record
   * @table-style default
   * @field-labels
   *   time: Date / Time
   *   message: Message
   * @default-fields time,message
   * @aliases dsdld
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields|void
   */
  public function showDebugLogs($deviceId, array $options = ['record' => NULL]) {
    if ($device = $this->loadDevice($deviceId)) {
      if ($options['record'] !== NULL) {
        $record = $device->getPlugin()->getRecord($options['record']) ?? '';
        if (!is_scalar($record)) {
          $record = json_encode($record, JSON_PRETTY_PRINT);
        }
        $this->io()->block($record);
      }
      else {
        $rows = $device->getPlugin()->showDebugLog($device);
        return new RowsOfFields($rows);
      }
    }
  }

  /**
   * Show error log.
   *
   * @usage digital-signage-device:log:error 17
   *   Show error logs from device with id 17.
   *
   * @command digital-signage-device:log:error
   * @param int $deviceId
   * @option record
   * @table-style default
   * @field-labels
   *   time: Date / Time
   *   message: Message
   * @default-fields time,message
   * @aliases dsdle
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields|void
   */
  public function showErrorLogs($deviceId, array $options = ['record' => NULL]) {
    if ($device = $this->loadDevice($deviceId)) {
      if ($options['record'] !== NULL) {
        $record = $device->getPlugin()->getRecord($options['record']) ?? '';
        if (!is_scalar($record)) {
          $record = json_encode($record, JSON_PRETTY_PRINT);
        }
        $this->io()->block($record);
      }
      else {
        $rows = $device->getPlugin()->showErrorLog($device);
        return new RowsOfFields($rows);
      }
    }
  }

  /**
   * Turn on debugging for the given device.
   *
   * @usage digital-signage-device:debug:on 17
   *   Turn on debugging on device with id 17.
   *
   * @command digital-signage-device:debug
   * @param int $deviceId
   * @aliases dsdd
   */
  public function debug($deviceId) {
    if ($device = $this->loadDevice($deviceId)) {
      $device->getPlugin()->debugDevice($device);
    }
  }

  /**
   * @param int $deviceId
   *
   * @return \Drupal\digital_signage_device\DeviceInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function loadDevice($deviceId) {
    /** @var \Drupal\digital_signage_device\DeviceInterface $device */
    $device = Device::load($deviceId);
    if (empty($device)) {
      throw new InvalidArgumentException('Incorrect device ID');
    }
    return $device;
  }

}
