<?php

namespace Drupal\digital_signage_custom_platform\Plugin\DigitalSignagePlatform;

use Drupal\digital_signage_device\DeviceInterface;
use Drupal\digital_signage_device\Entity\Device;
use Drupal\digital_signage_platform\PlatformPluginBase;

/**
 * Plugin implementation of the digital_signage_platform.
 *
 * @DigitalSignagePlatform(
 *   id = "custom",
 *   label = @Translation("Custom platform"),
 *   description = @Translation("Provides a custom platform.")
 * )
 */
class Custom extends PlatformPluginBase {

  /**
   * {@inheritdoc}
   */
  public function init() {
    // Nothing to do for now.
  }

  /**
   * {@inheritdoc}
   */
  public function scheduleBaseFields(array &$fields) {
  }

  /**
   * {@inheritdoc}
   */
  public function getPlatformDevices(): array {
    $this->messenger->addStatus('Receiving custom platform devices');

    $deviceEntities = [];

    foreach ($this->configFactory->get('digital_signage_custom_platform.settings')->get('devices') as $device) {
      $values = [
        'bundle' => $this->getPluginId(),
        'extid' => $device['id'],
        'title' => $device['name'],
        'status' => TRUE,
        'description' => $device['name'],
        'segments' => [],
      ];
      if (!empty($device['orientation']) && !empty($device['orientation']['width']) && !empty($device['orientation']['height'])) {
        $values['width'] = $device['orientation']['width'];
        $values['height'] = $device['orientation']['height'];
      }
      if (!empty($device['pin location']) && !empty($device['pin location']['x']) && !empty($device['pin location']['y'])) {
        $values['pin_location_x'] = $device['pin location']['x'];
        $values['pin_location_y'] = $device['pin location']['y'];
      }
      if (!empty($device['location'])) {
        if (!empty($device['location']['lat']) && !empty($device['location']['lng'])) {
          $values['geolocation'] = [
            'lat' => $device['location']['lat'],
            'lng' => $device['location']['lng'],
          ];
        }
        if (!empty($device['location']['perspective'])) {
          $values['perspective'] = $device['location']['perspective'];
        }
      }
      $deviceEntity = Device::create($values);
      $deviceEntities[] = $deviceEntity;
      if (!empty($device['segments'])) {
        foreach ($device['segments'] as $segment) {
          $deviceEntity->addSegment($segment);
        }
      }
    }

    return $deviceEntities;
  }

  /**
   * {@inheritdoc}
   */
  public function pushSchedule(DeviceInterface $device, $debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE) {
    // TODO: Implement pushSchedule() method.
  }

  /**
   * {@inheritdoc}
   */
  public function reloadSchedule(DeviceInterface $device, $debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE) {
    // TODO: Implement reloadSchedule() method.
  }

  /**
   * {@inheritdoc}
   */
  public function debugDevice(DeviceInterface $device) {
    // TODO: Implement debugDevice() method.
  }

  /**
   * {@inheritdoc}
   */
  public function showDebugLog(DeviceInterface $device) {
    // TODO: Implement showDebugLog() method.
  }

  /**
   * {@inheritdoc}
   */
  public function showErrorLog(DeviceInterface $device) {
    // TODO: Implement showErrorLog() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getScreenshot(DeviceInterface $device, $refresh = FALSE): array {
    // TODO: Implement getScreenshot() method.
    return [];
  }

}
