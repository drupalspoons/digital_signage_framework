<?php

namespace Drupal\digital_signage_schedule;

use Drupal\Core\Config\ImmutableConfig;

class DefaultDuration implements DurationInterface {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * DefaultDuration constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   */
  public function __construct(ImmutableConfig $settings) {
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getOffsetForCritical(): int {
    return $this->settings->get('offsets.critical');
  }

  /**
   * {@inheritdoc}
   */
  public function getOffsetByComplexity($complexityType): int {
    return ($complexityType === 'complex') ? $this->settings->get('offsets.complex') : 1;
  }

}
