<?php

namespace Drupal\digital_signage_schedule;

interface DurationInterface {

  /**
   * Returns the offset for critical entities.
   *
   * @return int
   */
  public function getOffsetForCritical(): int;

  /**
   * Returns the offset for the given complexity type.
   *
   * @param string $complexType
   *
   * @return int
   */
  public function getOffsetByComplexity($complexType): int;
}
