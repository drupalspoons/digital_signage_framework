<?php

namespace Drupal\digital_signage_schedule\Plugin\Action;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\digital_signage_schedule\ScheduleManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Pushes schedules to devices.
 *
 * @Action(
 *   id = "digital_signage_schedule_push",
 *   label = @Translation("Push schedules"),
 *   type = "digital_signage_device"
 * )
 */
class PushSchedules extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\digital_signage_schedule\ScheduleManager
   */
  protected $scheduleManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ScheduleManager $schedule_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->scheduleManager = $schedule_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('schedule.manager.digital_signage_platform')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResultAllowed::allowedIf($account !== NULL && $account->hasPermission('push digital signage schedule'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'force' => FALSE,
      'debugmode' => FALSE,
      'reloadassets' => FALSE,
      'reloadcontent' => FALSE,
      'entity_type' => NULL,
      'entity_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['force'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force update'),
      '#default_value' => $this->configuration['force'],
    ];
    $form['debugmode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#default_value' => $this->configuration['debugmode'],
    ];
    $form['reloadassets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reload assets (CSS, JS, fonts) on each schedule restart'),
      '#default_value' => $this->configuration['reloadassets'],
    ];
    $form['reloadcontent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reload content on each schedule restart'),
      '#default_value' => $this->configuration['reloadcontent'],
    ];

    $form['single_entity'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Single slide only'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $entity_types = [
      '' => '-- ' . $this->t('off') . ' --',
    ];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $entity_types[$definition->id()] = $definition->getLabel();
      }
    }
    $form['single_entity']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#default_value' => $this->configuration['entity_type'],
      '#options' => $entity_types,
    ];
    $form['single_entity']['entity_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Entity ID'),
      '#min' => 1,
      '#default_value' => $this->configuration['entity_id'],
      '#states' => [
        'invisible' => [
          'select[name="entity_type"]' => ['value' => ''],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['force'] = $form_state->getValue('force');
    $this->configuration['debugmode'] = $form_state->getValue('debugmode');
    $this->configuration['reloadassets'] = $form_state->getValue('reloadassets');
    $this->configuration['reloadcontent'] = $form_state->getValue('reloadcontent');
    $this->configuration['entity_type'] = NULL;
    $this->configuration['entity_id'] = NULL;
    if ($form_state->getValue('entity_type') !== '') {
      $this->configuration['entity_type'] = $form_state->getValue('entity_type');
      $this->configuration['entity_id'] = $form_state->getValue('entity_id');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute($device = NULL) {
    $this->scheduleManager->pushSchedules(
      $device->id(),
      $this->configuration['force'],
      $this->configuration['debugmode'],
      $this->configuration['reloadassets'],
      $this->configuration['reloadcontent'],
      $this->configuration['entity_type'],
      $this->configuration['entity_id']
    );
  }

}
