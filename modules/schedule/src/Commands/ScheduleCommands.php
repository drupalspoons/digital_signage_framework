<?php

namespace Drupal\digital_signage_schedule\Commands;

use Drupal\digital_signage_schedule\ScheduleManager;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile for digital signage devices.
 */
class ScheduleCommands extends DrushCommands {

  /**
   * @var \Drupal\digital_signage_schedule\ScheduleManager
   */
  protected $scheduleManager;

  /**
   * DigitalSignageScheduleCommands constructor.
   *
   * @param \Drupal\digital_signage_schedule\ScheduleManager $schedule_manager
   */
  public function __construct(ScheduleManager $schedule_manager) {
    $this->scheduleManager = $schedule_manager;
    parent::__construct();
  }

  /**
   * Command to push schedules and configuration to devices.
   *
   * @usage digital-signage-schedule:push
   *   Pushes schedules and configuration to devices.
   *
   * @command digital-signage-schedule:push
   * @option device
   * @option force
   * @option debugmode
   * @option reloadassets
   * @option reloadcontent
   * @option entitytype
   * @option entityid
   * @aliases dssp
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function pushSchedules(array $options = ['device' => NULL, 'force' => FALSE, 'debugmode' => FALSE, 'reloadassets' => FALSE, 'reloadcontent' => FALSE, 'entitytype' => NULL, 'entityid' => NULL]) {
    if ($options['entitytype'] !== NULL && $options['entityid'] === NULL) {
      $this->io()->error('Entity ID is required if entity type is given.');
      return;
    }
    $this->scheduleManager->pushSchedules($options['device'], $options['force'], $options['debugmode'], $options['reloadassets'], $options['reloadcontent'], $options['entitytype'], $options['entityid']);
  }

  /**
   * Command to reload schedules and configuration to devices.
   *
   * @usage digital-signage-schedule:reload
   *   Reload schedules and configuration on devices.
   *
   * @command digital-signage-schedule:reload
   * @option device
   * @option debugmode
   * @option reloadassets
   * @option reloadcontent
   * @aliases dssr
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function reloadSchedule(array $options = ['device' => NULL, 'debugmode' => FALSE, 'reloadassets' => FALSE, 'reloadcontent' => FALSE]) {
    $this->scheduleManager->reloadSchedules($options['device'], $options['debugmode'], $options['reloadassets'], $options['reloadcontent']);
  }

}
